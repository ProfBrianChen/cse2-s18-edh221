// Elizabeth Horan
// 2/12/18
// CSE 002
// hw03- Pyramid 

import java.util.Scanner;
public class Pyramid {
  public static void main(String[] args) {

  Scanner myScanner = new Scanner( System.in );
  System.out.println("The square side of the pyramid is (input length): ");
  //Get user input for square side of the pyramid  
  double lengthOfPyramid = myScanner.nextDouble(); 
   
  System.out.println("The height of the pyramid is (input height): ");
  //Get user input for height of the pyramid
  double heightOfPyramid = myScanner.nextDouble(); 
  
  //The number that lwh is divided by 
  double numberDivided = 3; 
    
  //The volume inside the pyramid is: 3125
  double volumeOfPyramid = (lengthOfPyramid * lengthOfPyramid * heightOfPyramid) / numberDivided;  
  System.out.println("volume of the pyramid: " + volumeOfPyramid);
  }
}
