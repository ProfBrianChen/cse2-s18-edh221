// Elizabeth Horan
// 2/12/18
// CSE 002
// hw03- Convert

import java.util.Scanner;
public class Convert {
  public static void main(String[] args) {

  Scanner myScanner = new Scanner( System.in );
  System.out.println("Enter the affected area in acres: ");
  //The affected area in acres: 23523.23
  double acresOfArea = myScanner.nextDouble(); 
    
  System.out.println("Enter the rainfall in the affected area: ");
  //The rainfall in the affected area in inches: 45
  double inchesOfRainfall = myScanner.nextDouble(); 
 
    
  //The rainfall in the affected area in cubic miles
  double gallons = acresOfArea * inchesOfRainfall;   
  double cubicMiles = gallons * (9.0816859724455 * Math.pow(10, -13));
  System.out.println("cubic miles: " + Math.round(cubicMiles * 100000000d)/100000000d);
  }
}