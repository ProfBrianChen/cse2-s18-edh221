// Elizabeth Horan
// 2/6/18
// CSE 002

public class WelcomeClass {
  public static void main(String[] args) {
    System.out.println("  -----------");
    System.out.println("  | Welcome |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-E--D--H--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
  }
}