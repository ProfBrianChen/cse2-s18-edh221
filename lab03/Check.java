// Elizabeth Horan
// 2/9/18
// CSE 2

  // The original cost of the check
  // The percentage tip they wish to pay
  // The number of ways the check will be split
  // How much each person in the group needs to spend in order to pay the check

import java.util.Scanner;
public class Check {
     // main method required for every Java program
  public static void main(String[] args) {
    
  // The original cost of the check
  Scanner myScanner = new Scanner( System.in );
  double checkCost = 54.69;
  System.out.println(" The original cost of the check: " + checkCost);
  
  // The percentage tip they wish to pay
  double tipPercent = 15;
  System.out.println(" Total tip percent: " + tipPercent);
 
  // The number of ways the check will be split
  int numPeople = 4;
  System.out.println(" Number of people splitting the check: " + numPeople);
    
  // How much each person in the group needs to spend in order to pay the check
  double totalCost = 62.88;
  double costPerPerson = 15.72;
  totalCost = (checkCost * tipPercent) + checkCost;
  costPerPerson = totalCost / numPeople;
  System.out.println(" How much each person in the group must pay: " + costPerPerson);
  }
}
