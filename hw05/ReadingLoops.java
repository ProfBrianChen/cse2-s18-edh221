// Elizabeth Horan
// 3/6/18
// CSE 002

import java.util.Scanner;
public class ReadingLoops {
  public static void main(String[] args) {
    
 
    
Scanner myScanner = new Scanner( System.in );
    
//input the course number 
boolean a = false; 
while ( !a ){
  System.out.println( "Input the course number" );
  int courseNumber = 0;
  if ( myScanner.hasNextInt() ){
    courseNumber = myScanner.nextInt();
    a = true; 
  }
  else {
    System.out.println( "Your input is invalid. Please enter an integer" );
    myScanner.next();
  }
}
  
//input the number of times the course meets in a week 
boolean b = false; 
while ( !b ){
  System.out.println( "Input the number of times the course meets in a week" );
  int meetingNumber = 0;
  if( myScanner.hasNextInt() ){
    meetingNumber = myScanner.nextInt();
    b = true; 
  }
  else {
    System.out.println( "Your input is invalid. Please enter an integer" );
    myScanner.next();
  }
}
    
//input the number of students in the class

boolean c = false; 
while ( !c ){
  System.out.println( "Input the number of students in the class" );
  int studentNumber = 0;
  if( myScanner.hasNextInt() ) {
    studentNumber = myScanner.nextInt();
    c = true; 
  } 
  else {
    System.out.println( "Your input is invalid. Please enter an integer" );
    myScanner.next();
  }
}
    

    
//input the department name
boolean d = false; 
while ( !d ){
  System.out.println( "Input the department name" );
  String departmentName = "";
  if ( myScanner.hasNext() ) {
    departmentName = myScanner.next();
    d = true;
  }
  else {
    System.out.println( "Your input is invalid. Please enter a string" );
  }
}
    
//input the instructor's name
boolean e = false;
while ( !e ) {
  System.out.println( "Input the instructor's name" );
  String instructorName = "";
  if ( myScanner.hasNext() ) {
    instructorName = myScanner.next(); 
    e = true;
  } 
  else {
    System.out.println( "Your input is invalid. Please enter a string" );
  }
}
 
//input the time the class starts
boolean f = false;
while ( !f ) {
  System.out.println( "Input the time the class starts" );
  String classTime = "";
  if ( myScanner.hasNext() ) {
    classTime = myScanner.next(); 
    f = true;
  } 
  else {
    System.out.println( "Your input is invalid. Please enter a string" );
  }
}
      
  }
}