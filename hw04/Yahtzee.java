// Elizabeth Horan
// 2/19/18
// CSE 002- Yahtzee

import java.util.Scanner;
public class Yahtzee {
  public static void main(String[] args) {
    
    
    
  //Enter 1 to input number or enter 2 to input random number
  Scanner myScanner = new Scanner( System.in );
  System.out.println( "Enter 1 to generate random number or enter 2 to input number" );
  double answer = myScanner.nextDouble(); 
  
   
  
  //If answer is 1, use the random number generator
  int upperBound = 6;
  int baseNumber = 1;
  int randomNumber1 = 0;
  int randomNumber2 = 0; 
  int randomNumber3 = 0; 
  int randomNumber4 = 0;
  int randomNumber5 = 0; 
  if( answer == 1 ){
    randomNumber1 = (int) (Math.random() * (upperBound)) + baseNumber;
    System.out.println("Random Number 1: " + randomNumber1);
    
    randomNumber2 = (int) (Math.random() * (upperBound)) + baseNumber;
    System.out.println("Random Number 2: " + randomNumber2);
    
    randomNumber3 = (int) (Math.random() * (upperBound)) + baseNumber;
    System.out.println("Random Number 3: " + randomNumber3);
    
    randomNumber4 = (int) (Math.random() * (upperBound)) + baseNumber;
    System.out.println("Random Number 4: " + randomNumber4);
    
    randomNumber5 = (int) (Math.random() * (upperBound)) + baseNumber;
    System.out.println("Random Number 5: " + randomNumber5);
      
    System.out.println("Random Numbers: " +  randomNumber1 + " " + randomNumber2 + " " + randomNumber3 + " " + randomNumber4 + " " + randomNumber5);
  }
    
    //If answer is 2, input 5 random numbers betwee 1 and 6
  if( answer == 2 ){
    System.out.println( "First Random Number: " );
    randomNumber1 = myScanner.nextInt();
    if( randomNumber1 < 1 || randomNumber1 > 6 ){
      System.out.println( "error" );
    }
    System.out.println( "Second Random Number: " );
    randomNumber2 = myScanner.nextInt();
    if( randomNumber2 < 1 || randomNumber2 > 6 ){
      System.out.println( "error" );
    }
    System.out.println( "Third Random Number: " );
    randomNumber3 = myScanner.nextInt();
    if( randomNumber3 < 1 || randomNumber3 > 6 ){
      System.out.println( "error" );
    }
    System.out.println( "Fourth Random Number: " );
    randomNumber4 = myScanner.nextInt();
    if( randomNumber4 < 1 || randomNumber4 > 6 ){
      System.out.println( "error" );
    }
    System.out.println( "Fifth Random Number: " );
    randomNumber5 = myScanner.nextInt();
    if( randomNumber5 < 1 || randomNumber5 > 6 ){
      System.out.println( "error" );
    }
  }
    
   
    //Upper section
    //Aces is the sum of all dice that come up one
    int acesSum = randomNumber1 + randomNumber2 + randomNumber3 + randomNumber4 + randomNumber5;
    int acesSum1 = 0;
    if ( randomNumber1 == 1){
      acesSum1 = acesSum1 + 1;
    }
    if ( randomNumber2 == 1){
      acesSum1 = acesSum1 + 1;
    }
    if ( randomNumber3 == 1){
      acesSum1 = acesSum1 + 1;
    }
    if ( randomNumber4 == 1){
      acesSum1 = acesSum1 + 1;
    }
    if ( randomNumber5 == 1){
      acesSum1 = acesSum1 + 1;
    }
    System.out.println("The number of dice that have a value of one is: " + acesSum1);

  
    
    //Twos is the sum of all dice that come up two 
    int twosSum = randomNumber1 + randomNumber2 + randomNumber3 + randomNumber4 + randomNumber5;
    int twosSum1 = 0;
    if ( randomNumber1 == 2){
      twosSum1 = twosSum1 + 1;
    }
    if ( randomNumber2 == 2){
      twosSum1 = twosSum1 + 1;
    }
    if ( randomNumber3 == 2){
      twosSum1 = twosSum1 + 1;
    }
    if ( randomNumber4 == 2){
      twosSum1 = twosSum1 + 1;
    }
    if ( randomNumber5 == 2){
      twosSum1 = twosSum1 + 1;
    }
    System.out.println("The number of dice that have a value of two is: " + twosSum1);
    
    
    
    //Threes is the sum of all dice that come up three
    int threesSum = randomNumber1 + randomNumber2 + randomNumber3 + randomNumber4 + randomNumber5;
    int threesSum1 = 0;
    if ( randomNumber1 == 3){
      threesSum1 = threesSum1 + 1;
    }
    if ( randomNumber2 == 3){
      threesSum1 = threesSum1 + 1;
    }
    if ( randomNumber3 == 3){
      threesSum1 = threesSum1 + 1;
    }
    if ( randomNumber4 == 3){
      threesSum1 = threesSum1 + 1;
    }
    if ( randomNumber5 == 3){
      threesSum1 = threesSum1 + 1;
    }
    System.out.println("The number of dice that have a value of three is: " + threesSum1);

    
    
    //Fours is the sum of all dice that come up four 
    int foursSum = randomNumber1 + randomNumber2 + randomNumber3 + randomNumber4 + randomNumber5;
    int foursSum1 = 0;
    if ( randomNumber1 == 4){
      foursSum1 = foursSum1 + 1;
    }
    if ( randomNumber2 == 4){
      foursSum1 = foursSum1 + 1;
    }
    if ( randomNumber3 == 4){
      foursSum1 = foursSum1 + 1;
    }
    if ( randomNumber4 == 4){
      foursSum1 = foursSum1 + 1;
    }
    if ( randomNumber5 == 4){
      foursSum1 = foursSum1 + 1;
    }
    System.out.println("The number of dice that have a value of four is: " + foursSum1);
    

    
    //Fives is the sum of all dice that come up five
    int fivesSum = randomNumber1 + randomNumber2 + randomNumber3 + randomNumber4 + randomNumber5;
    int fivesSum1 = 0;
    if ( randomNumber1 == 5){
      fivesSum1 = fivesSum1 + 1;
    }
    if ( randomNumber2 == 5){
      fivesSum1 = fivesSum1 + 1;
    }
    if ( randomNumber3 == 5){
      fivesSum1 = fivesSum1 + 1;
    }
    if ( randomNumber4 == 5){
      fivesSum1 = fivesSum1 + 1;
    }
    if ( randomNumber5 == 5){
      fivesSum1 = fivesSum1 + 1;
    }
    System.out.println("The number of dice that have a value of five is: " + fivesSum1);
    
    
    
    //Sixes is the sum of all dice that come up six 
    int sixesSum = randomNumber1 + randomNumber2 + randomNumber3 + randomNumber4 + randomNumber5;
    int sixesSum1 = 0;
    if ( randomNumber1 == 6){
      sixesSum1 = sixesSum1 + 1;
    }
    if ( randomNumber2 == 6){
      sixesSum1 = sixesSum1 + 1;
    }
    if ( randomNumber3 == 6){
      sixesSum1 = sixesSum1 + 1;
    }
    if ( randomNumber4 == 6){
      sixesSum1 = sixesSum1 + 1;
    }
    if ( randomNumber5 == 6){
      sixesSum1 = sixesSum1 + 1;
    }
    System.out.println("The number of dice that have a value of six is: " + sixesSum1 );
    
    
    
    //Three of a Kind is when exactly three dice are identical
    //Aces
    int threeOfAKind = 0;
    if ( acesSum1 == 3){
      System.out.println( "Three of a Kind: " + threeOfAKind + ( 1 * 3 ) );
    }
    //Twos 
    if ( twosSum1 == 3){
      System.out.println( "Three of a Kind: " + threeOfAKind + ( 2 * 3 ) );
    }
    //Threes
    if ( threesSum1 == 3){
      System.out.println( "Three of a Kind: " + threeOfAKind + ( 3 * 3 ) );
    }
    //Fours
    if ( foursSum1 == 3){
      System.out.println( "Three of a Kind: " + threeOfAKind + ( 4 * 3 ) );
    }
    //Fives
    if ( fivesSum1 == 3){
      System.out.println( "Three of a Kind: " + threeOfAKind + ( 5 * 3 ) );
    }
    //Sixes
    if ( sixesSum1 == 3){
      System.out.println( "Three of a Kind: " + threeOfAKind + (6 * 3 ) );
    }
    
    
    
    //Four of a Kind is when exactly four dice are identical
    //Aces
    int fourOfAKind = 0; 
    if ( acesSum1 == 4){
      System.out.println( "Four of a Kind: " + fourOfAKind + ( 1 * 4 ));
    }
    //Twos
    if ( twosSum1 == 4){
      System.out.println( "Four of a Kind: " + fourOfAKind + ( 2 * 4 ));
    }
    //Threes
    if ( threesSum1 == 4){
      System.out.println( "Four of a Kind: " + fourOfAKind + ( 3 * 4 ));
    }
    //Fours
    if ( foursSum1 == 4){
      System.out.println( "Four of a Kind: " + fourOfAKind + ( 4 * 4 ));
    }
    //Fives
    if ( fivesSum1 == 4){
      System.out.println( "Four of a Kind: " + fourOfAKind * ( 5 * 4 ));
    }
    //Sixes
    if ( sixesSum1 == 4){
      System.out.println( "Four of a Kind: " + fourOfAKind + ( 6 * 4 ));
    }

    
    
    //Yahtzee is when exactly all five dice are identical
    //Aces
    int yahtzee = 0; 
    if ( acesSum1 == 5){
      System.out.println( "Yahtzee: " + yahtzee + 50 );
    }
    //Twos
    if ( twosSum1 == 5){
      System.out.println( "Yahtzee: " + yahtzee + 50 );
    }
    //Threes
    if ( threesSum1 == 5){
      System.out.println( "Yahtzee: " + yahtzee + 50 );
    }
    //Fours
    if ( foursSum1 == 5){
      System.out.println( "Yahtzee: " + yahtzee + 50 );
    }
    //Fives
    if ( fivesSum1 == 5){
      System.out.println( "Yahtzee: " + yahtzee + 50 );
    }
    //Sixes
    if ( sixesSum1 == 5){
      System.out.println( "Yahtzee: " + yahtzee + 50 );
    }
    
    
    
    //Full house is when a pair of dice are identical and the other three are different but identical to themselves
    //Aces
    int fullHouse = 0; 
    if ( acesSum1 == 2 && twosSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( acesSum1 == 2 && threesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( acesSum1 == 2 && foursSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( acesSum1 == 2 && fivesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( acesSum1 == 2 && sixesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    //Twos
    if ( twosSum1 == 2 && acesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( twosSum1 == 2 && threesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( twosSum1 == 2 && foursSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( twosSum1 == 2 && fivesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( twosSum1 == 2 && sixesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    //Threes
    if ( threesSum1 == 2 && acesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( threesSum1 == 2 && twosSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( threesSum1 == 2 && foursSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( threesSum1 == 2 && fivesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( threesSum1 == 2 && sixesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    //Fours
    if ( foursSum1 == 2 && acesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( foursSum1 == 2 && twosSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( foursSum1 == 2 && threesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( foursSum1 == 2 && fivesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( foursSum1 == 2 && sixesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    //Fives
    if ( fivesSum1 == 2 && acesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( fivesSum1 == 2 && twosSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( fivesSum1 == 2 && threesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( fivesSum1 == 2 && foursSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( fivesSum1 == 2 && fivesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    //Sixes
    if ( sixesSum1 == 2 && acesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( sixesSum1 == 2 && twosSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( sixesSum1 == 2 && threesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( sixesSum1 == 2 && foursSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    if ( sixesSum1 == 2 && fivesSum1 == 3 ){
      System.out.println( "Full House: " + fullHouse + 25 );
    }
    
    
    
    //Small Straight is when a four dice exhibit sequential values 
    //e.g. 1-2-3-4, 2-3-4-5, or 3-4-5-6, in which case its value is 30. 
    int smallStraight = 30;
    //For the sequence 1-2-3-4
    if ( randomNumber1 == 1 && randomNumber2 == 2 && randomNumber3 == 3 && randomNumber4 == 4 ){
      System.out.println( "Small Straight: " + smallStraight );
    }
    if ( randomNumber2 == 1 && randomNumber3 == 2 && randomNumber4 == 3 && randomNumber5 == 4 ){
      System.out.println( "Small Straight: " + smallStraight );
    }
    if ( randomNumber3 == 1 && randomNumber4 == 2 && randomNumber5 == 3 && randomNumber1 == 4 ){
      System.out.println( "Small Straight: " + smallStraight );
    }
    if ( randomNumber4 == 1 && randomNumber5 == 2 && randomNumber1 == 3 && randomNumber2 == 4 ){
      System.out.println( "Small Straight: " + smallStraight );
    }
    if ( randomNumber5 == 1 && randomNumber1 == 2 && randomNumber2 == 3 && randomNumber3 == 4 ){
      System.out.println( "Small Straight: " + smallStraight );
    }
    
    //For the sequence 2-3-4-5
    if ( randomNumber1 == 2 && randomNumber2 == 3 && randomNumber3 == 4 && randomNumber4 == 5 ){
      System.out.println( "Small Straight: " + smallStraight );
    }
    if ( randomNumber2 == 2 && randomNumber3 == 3 && randomNumber4 == 4 && randomNumber5 == 5 ){
      System.out.println( "Small Straight: " + smallStraight );
    }
    if ( randomNumber3 == 2 && randomNumber4 == 3 && randomNumber5 == 4 && randomNumber1 == 5 ){
      System.out.println( "Small Straight: " + smallStraight );
    }
    if ( randomNumber4 == 2 && randomNumber5 == 3 && randomNumber1 == 4 && randomNumber2 == 5 ){
      System.out.println( "Small Straight: " + smallStraight );
    }
    if ( randomNumber5 == 2 && randomNumber1 == 3 && randomNumber2 == 4 && randomNumber3 == 5 ){
      System.out.println( "Small Straight: " + smallStraight );
    }
    
    //For the sequence 3-4-5-6
    if ( randomNumber1 == 3 && randomNumber2 == 4 && randomNumber3 == 5 && randomNumber4 == 6 ){
      System.out.println( "Small Straight: " + smallStraight );
    }
    if ( randomNumber2 == 3 && randomNumber3 == 4 && randomNumber4 == 5 && randomNumber5 == 6 ){
      System.out.println( "Small Straight: " + smallStraight );
    }
    if ( randomNumber3 == 3 && randomNumber4 == 4 && randomNumber5 == 5 && randomNumber1 == 6 ){
      System.out.println( "Small Straight: " + smallStraight );
    }
    if ( randomNumber4 == 3 && randomNumber5 == 4 && randomNumber1 == 5 && randomNumber2 == 6 ){
      System.out.println( "Small Straight: " + smallStraight );
    }
    if ( randomNumber5 == 3 && randomNumber1 == 4 && randomNumber2 == 5 && randomNumber3 == 6 ){
      System.out.println( "Small Straight: " + smallStraight );
    }
    
    
    
    //Large Straight is when all five dice exhibit sequential values 
    int largeStraight = 40; 
    //For the sequence 1-2-3-4-5 
    if ( randomNumber1 == 1 && randomNumber2 == 2 && randomNumber3 == 3 && randomNumber4 == 4 && randomNumber5 == 5 ){
      System.out.println( "Large Straight: " + largeStraight );
    }
    if ( randomNumber2 == 1 && randomNumber3 == 2 && randomNumber4 == 3 && randomNumber5 == 4 && randomNumber1 == 5 ){
      System.out.println( "Large Straight: " + largeStraight );
    }
    if ( randomNumber3 == 1 && randomNumber4 == 2 && randomNumber5 == 3 && randomNumber1 == 4 && randomNumber2 == 5 ){
      System.out.println( "Large Straight: " + largeStraight );
    }
    if ( randomNumber4 == 1 && randomNumber5 == 2 && randomNumber1 == 3 && randomNumber2 == 4 && randomNumber3 == 5 ){
      System.out.println( "Large Straight: " + largeStraight );
    }
    if ( randomNumber5 == 1 && randomNumber1 == 2 && randomNumber2 == 3 && randomNumber3 == 4 && randomNumber4 == 5 ){
      System.out.println( "Large Straight: " + largeStraight );
    }
    
    //For the sequence 2-3-4-5-6 
    if ( randomNumber1 == 2 && randomNumber2 == 3 && randomNumber3 == 4 && randomNumber4 == 5 && randomNumber5 == 6 ){
      System.out.println( "Large Straight: " + largeStraight );
    }
    if ( randomNumber2 == 2 && randomNumber3 == 3 && randomNumber4 == 4 && randomNumber5 == 5 && randomNumber1 == 6 ){
      System.out.println( "Large Straight: " + largeStraight );
    }
    if ( randomNumber3 == 2 && randomNumber4 == 3 && randomNumber5 == 4 && randomNumber1 == 5 && randomNumber2 == 6 ){
      System.out.println( "Large Straight: " + largeStraight );
    }
    if ( randomNumber4 == 2 && randomNumber5 == 3 && randomNumber1 == 4 && randomNumber2 == 5 && randomNumber3 == 6 ){
      System.out.println( "Large Straight: " + largeStraight );
    }
    if ( randomNumber5 == 2 && randomNumber1 == 3 && randomNumber2 == 4 && randomNumber3 == 5 && randomNumber4 == 6 ){
      System.out.println( "Large Straight: " + largeStraight );
    }
    
 
    
    //Chance is equal to the sum of the value on all six dice
    int chance = randomNumber1 + randomNumber2 + randomNumber3 + randomNumber4 + randomNumber5; 
    System.out.println( "Chance: " + chance );

    
    
    //Upper Section Initial Total
    int upperSectionInitialTotal = ( acesSum1 * 1) + ( twosSum1 * 2 ) + ( threesSum1 * 3 ) + ( foursSum1 * 4 ) + ( fivesSum1 * 5 ) + ( sixesSum1 * 6 );
    System.out.println( "Upper Section Initial Total: " + upperSectionInitialTotal );
    
    
    //Upper Section Total Including Bonus 
    if( upperSectionInitialTotal > 63 ){
      upperSectionInitialTotal = upperSectionInitialTotal + 35;
    }
    
    
    //Lower Section Total
    int lowerSectionTotal = threeOfAKind + fourOfAKind + yahtzee + fullHouse + smallStraight + largeStraight + chance;
    System.out.println( "Lower Section Total: " + lowerSectionTotal );
    
    
    //Grand Total- the sum of the Upper Section Total Including Bonus and the Lower Section Total
    int grandTotal = upperSectionInitialTotal + lowerSectionTotal;
    System.out.println( "Grand Total: " + grandTotal );
  }  
} 
  


