// Elizabeth Horan
// 2/2/18
// CSE 2
// The purpose of this lab is to store numerical values in variables and perform basic computations with those variables.
// MPG is miles per gallon. 
public class Cyclometer {
    // main method required for every Java program
   	public static void main(String[] args) {
    
    // Variables 
    int secsTrip1=480;  // Trip 1 is 480 seconds
   	int secsTrip2=3220;  // Trip 2 is 3220 seconds
    int countsTrip1=1561;  // Trip 1 is 1561 counts
    int countsTrip2=9037; // Trip 2 is 9037 counts
   
    double wheelDiameter=27.0,  // Wheel diameter is 27
  	PI=3.14159, //
  	feetPerMile=5280,  // Feet per mile is 5280
  	inchesPerFoot=12,   // Inches per foot is 12
  	secondsPerMinute=60;  // Seconds per minute is 60
    double distanceTrip1, distanceTrip2,totalDistance;  // Total distance 
    
		// Printing eqations
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute) + " minutes and had: " + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had: " + countsTrip2 + " counts.");
      
  distanceTrip1=countsTrip1*wheelDiameter*PI;
  // Above gives distance in inches
  //(for each count, a rotation of the wheel travels
  //the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	totalDistance=distanceTrip1+distanceTrip2;
      
  //Print out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");
	}  //end of main method   
} //end of class