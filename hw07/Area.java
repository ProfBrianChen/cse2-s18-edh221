// Elizabeth Horan
// 3/27/18
// CSE 002

import java.util.Scanner;
public class Area {
  public static void main(String[] args) {

  Scanner myScanner = new Scanner( System.in );
  
  //Ask the user whether they would like the area of a rectangle, triangle or circle. 
  String shape;
  boolean x = false;
  while ( !x ) {
    System.out.println( "Input the type of shape you want the area of." );
    shape = myScanner.next();
     
    
    //If the user chooses a rectangle
    if ( shape.equals( "rectangle" ) ) {
      double length = getInput( myScanner, "Input length" );
      double width = getInput( myScanner, "Input width" );
      double area = rectangleArea( length, width );
      System.out.println( "area of the rectangle: " + area ); 
      x = true; 
    }
    
 
    // If the user chooses a triangle
    else if ( shape.equals( "triangle" ) ) {
    double height = getInput( myScanner, "Input height" );
    double base = getInput( myScanner, "Input base" );
    double area = triangleArea( height, base ); 
    System.out.println( "area of the triangle: " + area );
    x = true; 
    }
    
    
    //If the user chooses a circle
    else if ( shape.equals( "circle" ) ){
    double radius = getInput( myScanner, "Input radius" );
    double pi = 3.1415926535897932384626433832795;
    double area = circleArea( radius, pi ); 
    System.out.println( "area of the circle: " + area );
    x = true; 
    }
  
    else {
    System.out.println( "That is an unacceptable shape. You must enter a rectangle, triangle, or circle." );
    }
    
  }
  }
  
  
  
  //rectangle method
  public static double rectangleArea( double length, double width ) {
    double area = length * width; 
    return area; 
  }
  
  
  
  //triangle method
  public static double triangleArea( double height, double base ) {
    double area = ( height * base ) / 2;
    return area;
  }
  
  
  
  //circle method 
  public static double circleArea( double radius, double pi ) {
    double area = ( radius * radius ) * pi;
    return area;
  }
 
  
  
  public static double getInput( Scanner dimension, String prompt ) {
    boolean y = false;
    while ( !y ) {
      System.out.println( prompt );
      if (dimension.hasNextDouble() ) {
        double x = dimension.nextDouble();
        return x;
      }
      else {
        System.out.println( "Your input is invalid. Please enter a double." );
        dimension.next();
      }
    } 
    return 0;
  }
  
  
  
  
}