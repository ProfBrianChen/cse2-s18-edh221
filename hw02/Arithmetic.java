// Elizabeth Horan
// 2/6/18
// CSE 002

public class Arithmetic {
  public static void main(String[] args) {
//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;
    
//Total cost of each kind of item
double totalCostOfPants = numPants * pantsPrice;   //total cost of pants
System.out.println("total cost of pants: " + totalCostOfPants);
    
double totalCostOfShirts = numShirts * shirtPrice;   //total cost of sweatshirts
System.out.println("total cost of shirts: " + totalCostOfShirts);
    
double totalCostOfBelts = numBelts * beltCost;   //total cost of belts
System.out.println("total cost of belts: " + totalCostOfBelts);

//Sales tax charged buying all of each kind of item    
double totalSalesTaxOfPants = paSalesTax * totalCostOfPants; 
totalSalesTaxOfPants = Math.round(totalSalesTaxOfPants * 100.0)/100.0;
System.out.println("total sales tax of pants: " + totalSalesTaxOfPants);
    
double totalSalesTaxOfShirts = paSalesTax * totalCostOfShirts;
totalSalesTaxOfShirts = Math.round(totalSalesTaxOfShirts * 100.0)/100.0;
System.out.println("total sales tax of shirts: " + totalSalesTaxOfShirts);
    
double totalSalesTaxOfBelts = paSalesTax * totalCostOfBelts;
totalSalesTaxOfBelts = Math.round(totalSalesTaxOfBelts * 100.0)/100.0;
System.out.println("total sales tax of belts: " + totalSalesTaxOfBelts);
    
//Total cost of purchases (before tax)    
double totalCostOfPurchase = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
System.out.println("total cost of purchase: " + totalCostOfPurchase);
    
//Total sales tax
double totalSalesTax = totalSalesTaxOfPants + totalSalesTaxOfShirts + totalSalesTaxOfBelts;
System.out.println("total sales tax: " + totalSalesTax);
    
//Total paid for this transaction, including sales tax    
double totalPaidForTransaction = totalCostOfPurchase + totalSalesTax;
System.out.println("total paid for transaction: " + totalPaidForTransaction);
    
/*
Total cost of each kind of item
Sales tax charged buying all of each kind of item
Total cost of purchases (before tax)
Total sales tax
Total paid for this transaction, including sales tax
*/
  }
}
