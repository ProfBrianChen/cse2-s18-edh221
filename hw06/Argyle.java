// Elizabeth Horan
// 3/20/18
// CSE 002

import java.util.Scanner;
public class Argyle {
  public static void main(String[] args) {
  
  Scanner myScanner = new Scanner( System.in );

  int viewingWidth = 0;
	int viewingHeight = 0;
	int diamondWidth = 0;
	int centerWidth = 0;
  boolean x = false;

  while ( !x ) {
	  //Ask the user to enter a positive integer for the width of the viewing window
    System.out.println( "Please enter a positive integer for the width of the viewing window" );
    if ( !myScanner.hasNextInt() ){
      System.out.println( "Your input is invalid" );
      myScanner.next();
      continue; 
    }
    viewingWidth = myScanner.nextInt();
    if ( !(viewingWidth > 0) ) {
      System.out.println( "Your input is in the wrong range" );
      x = false;
      continue;
    }
    x = true;



	  //Ask the user to enter a positive integer for the height of the viewing window  
	  System.out.println( "Please enter a positive integer for the height of the viewing window" );
	  if ( !myScanner.hasNextInt() ){
      System.out.println( "Your input is invalid" );
      myScanner.next();
      continue; 
    }
    viewingHeight = myScanner.nextInt();
    if ( !(viewingHeight > 0) ) {
      System.out.println( "Your input is in the wrong range" );
      x = false;
      continue;
    }
    x = true;


    //Ask the user to enter a positive integer for the width of the argyle diamonds
    System.out.println( "Please enter a positive integer for the width of the argyle diamonds" );
	    if ( !myScanner.hasNextInt() ){
        System.out.println( "Your input is invalid" );
        myScanner.next();
      continue; 
    }
    diamondWidth = myScanner.nextInt();
    if ( !(diamondWidth > 0) ) {
      System.out.println( "Your input is in the wrong range" );
      x = false;
      continue;
    }
    x = true;
	
	
	
	  //Ask the user to enter a positive odd integer for the width of the argyle center stripe
	  System.out.println( "Please enter a positive odd integer for the width of the argyle center stripe" );
	  if ( !myScanner.hasNextInt() ){
      System.out.println( "Your input is invalid" );
      myScanner.next();
      continue; 
    }
    centerWidth = myScanner.nextInt();
    if ( !(centerWidth > 0) ) {
      System.out.println( "Your input is in the wrong range" );
      x = false;
      continue;
    }
    x = true;

	
	
	  //Ask the user to enter a first character for the pattern fill
    System.out.println( "Please enter a first character for the pattern fill" );
	  String firstCharacter = myScanner.next();

	
	
    //Ask the user to enter a second character for the pattern fill
    System.out.println( "Please enter a second character for the pattern fill" );
	  String secondCharacter = myScanner.next();

	
	
    //Ask the user to enter a third character for the stripe fill
    System.out.println( "Please enter a third character for the pattern fill" );
    String thirdCharacter = myScanner.next();
	
	
  }
	
	
	
  }
}